import model
from components.merging import MergeProcess
from components.providers.providers_list import PROVIDERS_LIST
from components.service import BaseComponent, GetInternalModel, SyncUtils
from components.updating import SendDataProcess
from providers.everbridge import threadlocal


class UserSyncProcessor(BaseComponent):
    def __init__(self, sync_id):
        BaseComponent.__init__(self, sync_id, {"component_id": 2})

    def __sync_user__(self, user):
        user_id = user["active_directory_user_id"]
        deleted = threadlocal.sync_data["sync_data"]["deactivated"]

        try:
            data_from_all_providers = self.collect_data_for_all_providers(user)
            prepared_data = GetInternalModel(self.sync_id, user_id).get_model(data_from_all_providers)
            merged_data = MergeProcess(self.sync_id, user_id).merge(prepared_data)
            SendDataProcess(self.sync_id).send_data_to_remote_system(merged_data, user_id, prepared_data)
            if user_id in deleted:
                model.ActiveDirectoryUser.delete_active_directory_user(user_id)
                self.log({"message": "User was deactivated and deleted from local store",
                          "active_directory_user_id": user_id,
                          "success": False,
                          "operation": "__deactivate__"})
            return True
        except Exception as e:
            self.log({"message": "Error syncing user",
                      "active_directory_user_id": user_id,
                      "success": False,
                      "operation": "__collect__",
                      "data_1": str(e),
                      "data_2": str(user)})

            # DON'T RAISE EXCEPTION. TRYING TO SYNC OTHER USER
            return False

    def collect_data_for_all_providers(self, user):
        system_result = {}
        for provider_class in PROVIDERS_LIST:
            provider = provider_class(self.sync_id)
            data = SendDataProcess.collect_data_from_provider(provider, user)
            if data:
                system_result[provider.system_id] = data
        return system_result

    def __sync_users__(self):
        for user in model.ActiveDirectoryUser.get_all_users():
            if user['email'].lower():
                time, result = SyncUtils.mesure(lambda: self.__sync_user__(user))
                self.log({"message": "sync user",
                          "success": result,
                          "operation": "sync user process",
                          "active_directory_user_id": user["active_directory_user_id"],
                          "execution_time": time})
        return True

    def start_sync(self):
        time, result = SyncUtils.mesure(lambda: self.__sync_users__())
        self.log({"message": "Users successfully synced",
                  "success": True,
                  "operation": "syncing users",
                  "execution_time": time})
